cmake_minimum_required(VERSION 2.8)
project( SNPEMobileNetSSDCv )
find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} /home/linaro/snpe-1.30.0.480/include/zdl )
link_directories(/home/linaro/snpe-1.30.0.480/lib/aarch64-linux-gcc4.9/ )
add_executable(SNPEMobileNetSSDCv
	main.cpp
)

target_link_libraries( SNPEMobileNetSSDCv ${OpenCV_LIBS} -lSNPE -lcdsprpc -lsymphony-cpu)
